let IP = "3.0.19.196";
let PORT = "9191";
let urlUser = `http://${IP}:${PORT}/api/user/login`;
let urlInfo = `http://${IP}:${PORT}/api/user/getInfoUser`;
let urlHP = `http://${IP}:${PORT}/api/dkhp/getDanhSachLopHocPhanCoTrongHocPhan?maHocPhan=1&maHocKi=1`;
let accessToken;

const sendHttpRequest = (method, url, data, accessToken) => {
  if (data === null || data === "") {
    return fetch(url, {
      method: method,
      headers: {
        "Content-Type": "application/json",
        Authorization: accessToken,
      },
    })
      .then((res) => {
        return res.text().then((text) => (text ? JSON.parse(text) : {}));
      })
      .catch((error) => console.log("ERROR!", error));
  } else {
    return fetch(url, {
      method: method,
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: accessToken,
      },
    })
      .then((res) => {
        return res.text().then((text) => (text ? JSON.parse(text) : {}));
      })
      .catch((error) => console.log("ERROR!", error));
  }
};

const login = () => {
  sendHttpRequest("POST", urlUser, {
    mssv: "17087871",
    password: "123456",
  }).then((responseData) => {
    accessToken = responseData.data.accessToken;
    console.log(responseData);
  });
};

const getInfoUser = () => {
  sendHttpRequest("GET", urlInfo, "", accessToken).then((responseData) =>
    console.log(responseData)
  );
};

const getHocPhan = () => {
  sendHttpRequest("GET", urlHP).then((responseData) =>
    console.log(responseData.data.dsLopHocPhan)
  );
};
